const express = require("express");

const app = express();

const port = 4000;

app.use(express.json());

let users = [

	{
	  	email: "mighty12@gmail.com",
	  	username: "mightyMouse12",
	  	password: "notrelatedtomickey",
	  	isAdmin: false
	},
	{
		email: "minnieMouse@gmail.com",
		username: "minniexmickey",
		password: "minniesincethestart",
		isAdmin: false
	},
	{
		email: "mickeyTheMouse@gmail.com",
		username: "mickeyKing",
		password: "thefacethatrunstheplace",
		isAdmin: true
	}
	
];

let items = [];

let loggedUser;


// view users
app.get('/users',(req,res) => {

	console.log(users)

	res.send(users)
})

//register
app.post('/users',(req,res)=>{

	
	console.log(req.body);

	
	let newUser = {

		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin

	};

	users.push(newUser);
	console.log(users);

	res.send("Registered Successfully.");


})

//login
app.post('/users/login',(req,res)=>{

	
	console.log(req.body);

	
	let foundUser = users.find((user)=>{

		return user.username === req.body.username && user.password === req.body.password;


	});


	if(foundUser !== undefined){
		
		loggedUser = foundUser;

		console.log(loggedUser);;

		res.send(`Thank you for logging in ${req.body.username}`);
	} else {

		loggedUser = foundUser;
		res.send('Login failed. Wrong Credentials');

	}



})



//addItem
app.post('/items',(req,res)=>{

	console.log(loggedUser);
	console.log(req.body);
	
	if(loggedUser.isAdmin === true){
		let newItem = {

			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			isActive: req.body.isActive,

		};

		items.push(newItem);
		console.log(items);

		res.send('You have added a new item.');

	} else {

		res.send('Unauthorized. Action Forbidden.');

	}

})



//getAllItem
app.get('/items',(req,res)=>{
	console.log(loggedUser);

	if(loggedUser.isAdmin === true){

		res.send(items);

	} else {
	
		res.send('Unauthorized. Action Forbidden.');

	}

})

// get singleItem by its index
app.get('/items/:index',(req,res)=>{
	console.log(req.params);
	console.log(req.params.index);
	let index = parseInt(req.params.index)
	let item = items[index];
	res.send(item);
	})

// edit/update the  price
app.put('/items/editPrice/:index',(req,res)=>{

	console.log(req.params);
	console.log(req.params.index);
	let itemIndex = parseInt(req.params.index);
	if(loggedUser.isAdmin === true){

		items[itemIndex].price = req.body.price;
		console.log(items[itemIndex]);
		res.send('Item Price Edited.')
		
	} else {

		res.send('Unauthorized: Action Forbidden.')

	}
})

// update the user  to be an admin
app.put('/users/admin/:index',(req,res)=>{

	console.log(req.params);
	console.log(req.params.index);
	let usersIndex = parseInt(req.params.index);
	if(loggedUser.isAdmin === true){

		users[usersIndex].isAdmin = req.body.isAdmin;
		console.log(users[usersIndex]);
		res.send('User Authenticated Admin.')
	} else {

		res.send('Unauthorized: Action Forbidden.')

	}
})

app.listen(port, ()=>console.log(`Server is running at port ${port}`));